import cors from '@fastify/cors';
import fastifyEnv from '@fastify/env';
import fastify, { FastifyInstance } from 'fastify';
import { readFileSync } from 'fs';
import { config, logger } from './config';
import { productRoutes } from './modules/product/product.routes';
import { productSchemas } from './modules/product/product.schema';
import { replacementRoutes } from './modules/replacement/replacement.routes';
import { replacementSchemas } from './modules/replacement/replacement.schema';

function registerSchemas(server: FastifyInstance) {
  for (const schema of [...productSchemas, ...replacementSchemas]) {
    server.addSchema(schema);
  }
}

function registerRoutes(server: FastifyInstance) {
  server.get('/healthcheck', async function () {
    return { status: 'OK' };
  });
  server.register(productRoutes, { prefix: 'api/products' });
  server.register(replacementRoutes, { prefix: 'api/replacements' });
}

async function init(server: FastifyInstance) {
  registerSchemas(server);
  registerRoutes(server);

  server.register(fastifyEnv, config);
  server.register(cors);

  await server.after();
}

async function main() {
  const https = {
    key: readFileSync('./key.pem'),
    cert: readFileSync('./cert.pem'),
  };

  try {
    const server = parseInt(process.env.OMIT_SSL || '0') > 0 ? fastify({ logger }) : fastify({ logger, https });
    await init(server);
    await server.listen({
      host: process.env.HOST,
      port: +(process.env.PORT || 8080),
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

main();
