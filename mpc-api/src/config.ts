export const config = {
  confKey: 'config',
  dotenv: true,
  data: process.env,
  schema: {
    type: 'object',
    required: ['HOST', 'PORT', 'DB'],
    properties: {
      HOST: { type: 'string' },
      PORT: { type: 'number' },
      DB: { type: 'string' },
    },
  },
};

export const logger = {
  transport: {
    target: '@fastify/one-line-logger',
  },
};
