export type SearchProductsQuery = {
  search?: string;
};

export type GetProductQuery = {
  id: string;
  manufacturer: string;
};
