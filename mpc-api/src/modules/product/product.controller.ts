import { FastifyReply, FastifyRequest } from 'fastify';
import * as products from './product.service';
import { GetProductQuery, SearchProductsQuery } from './product.types';

export async function searchProducts(req: FastifyRequest, rep: FastifyReply) {
  const { search } = req.query as SearchProductsQuery;
  const results = await products.find(search);
  rep.code(200).send(results);
}

export async function getProduct(req: FastifyRequest, rep: FastifyReply) {
  const { id, manufacturer } = req.query as GetProductQuery;
  const results = await products.get(id, manufacturer);
  rep.code(200).send(results);
}
