import { FastifyInstance } from 'fastify';
import { getProduct, searchProducts } from './product.controller';
import { $ref } from './product.schema';

export async function productRoutes(server: FastifyInstance) {
  server.get(
    '/',
    {
      schema: {
        querystring: $ref('productSearchQueryParamsSchema'),
        response: {
          200: $ref('productsResponseSchema'),
        },
      },
    },
    searchProducts,
  );

  server.get(
    '/details',
    {
      schema: {
        querystring: $ref('productQueryParams'),
        response: {
          200: $ref('productResponseSchema'),
        },
      },
    },
    getProduct,
  );
}
