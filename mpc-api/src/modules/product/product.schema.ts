import { buildJsonSchemas } from 'fastify-zod';
import { z } from 'zod';

const productSearchQueryParamsSchema = z.object({
  search: z.string().optional(),
});

const productQueryParams = z.object({
  id: z.string(),
  manufacturer: z.string(),
});

const productSchema = {
  id: z.string(),
  manufacturer: z.string(),
  name: z.string(),
  description: z.string(),
  image: z.string(),
};

const productResponseSchema = z.object({ ...productSchema });

const productsResponseSchema = z.array(productResponseSchema);

export const { schemas: productSchemas, $ref } = buildJsonSchemas(
  {
    productSearchQueryParamsSchema,
    productQueryParams,
    productResponseSchema,
    productsResponseSchema,
  },
  { $id: 'ProductSchemas' },
);
