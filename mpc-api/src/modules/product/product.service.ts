import prisma from '../../utils/prisma';

export async function find(search?: string) {
  let where = {};

  if (search) {
    where = {
      OR: [
        {
          id: {
            contains: search,
          },
        },
        {
          manufacturer: {
            contains: search,
          },
        },
        {
          name: {
            contains: search,
          },
        },
      ],
    };
  }

  return prisma.product.findMany({ where, orderBy: { name: 'asc' } });
}

export async function get(id: string, manufacturer: string) {
  return prisma.product.findFirstOrThrow({ where: { id, manufacturer } });
}
