export type ReplacementProductInfo = {
  productName: string;
  manufacturer: string;
};

export type GetReplacementsQuery = {
  productName: string;
  manufacturer: string;
  depth: number;
};
