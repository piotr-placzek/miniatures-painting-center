import { FastifyInstance } from 'fastify';
import { searchReplacements } from './replacement.controller';
import { $ref } from './replacement.schema';
import { $ref as $pRef } from '../product/product.schema'

export async function replacementRoutes(server: FastifyInstance) {
  server.get(
    '/',
    {
      schema: {
        querystring: $ref('replacementsSearchQueryParamsSchema'),
        response: {
          200: $pRef('productsResponseSchema'),
        },
      },
    },
    searchReplacements,
  );
}
