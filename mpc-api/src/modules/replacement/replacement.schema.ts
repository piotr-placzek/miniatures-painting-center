import { buildJsonSchemas } from 'fastify-zod';
import { z } from 'zod';

const replacementsSearchQueryParamsSchema = z.object({
  productName: z.string(),
  manufacturer: z.string(),
  depth: z.number().default(0),
});

const replacementSchema = {
  id: z.number(),
  manufacturerA: z.string(),
  productNameA: z.string(),
  manufacturerB: z.string(),
  productNameB: z.string(),
  author: z.string(),
};

const replacementResponseSchema = z.object(replacementSchema);
const replacementsResponseSchema = z.array(replacementResponseSchema);

export const { schemas: replacementSchemas, $ref } = buildJsonSchemas(
  {
    replacementsSearchQueryParamsSchema,
    replacementResponseSchema,
    replacementsResponseSchema,
  },
  { $id: 'ReplacementSchemas' },
);
