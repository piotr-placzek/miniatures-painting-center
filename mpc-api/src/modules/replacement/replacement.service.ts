import { Product, Replacement } from '@prisma/client';
import prisma from '../../utils/prisma';
import { ReplacementProductInfo } from './replacement.types';

export async function find(productName: string, manufacturer: string, depth: number) {
  const replacementsProductsInfo: ReplacementProductInfo[] = await deepSearch([{ productName, manufacturer }], depth);

  const results: Product[] = [];
  for (let i = 0; i < replacementsProductsInfo.length; ++i) {
    const product = await prisma.product.findFirst({
      where: {
        manufacturer: replacementsProductsInfo[i].manufacturer,
        name: replacementsProductsInfo[i].productName,
      },
    });
    if (product) {
      results.push(product);
    }
  }

  return results;
}

async function deepSearch(productsInfo: ReplacementProductInfo[], depth: number) {
  const results: ReplacementProductInfo[] = [];
  for (let i = 0; i < productsInfo.length; ++i) {
    results.push(
      ...(
        await prisma.replacement.findMany({
          where: {
            OR: [
              {
                manufacturerA: productsInfo[i].manufacturer,
                productNameA: productsInfo[i].productName,
              },
              {
                manufacturerB: productsInfo[i].manufacturer,
                productNameB: productsInfo[i].productName,
              },
            ],
          },
          orderBy: { id: 'asc' },
        })
      ).map((r: Replacement) => getReplacementInfo(productsInfo[i], r)),
    );
  }
  if (depth) {
    await deepSearch(results, depth - 1);
  }
  return results.filter(removeDuplicatedReplacementsFilter);
}

function getReplacementInfo(baseProductInfo: ReplacementProductInfo, entity: Replacement): ReplacementProductInfo {
  if (entity.manufacturerA === baseProductInfo.manufacturer && entity.productNameA === baseProductInfo.productName) {
    return {
      manufacturer: entity.manufacturerB,
      productName: entity.productNameB,
    };
  } else if (
    entity.manufacturerB === baseProductInfo.manufacturer &&
    entity.productNameB === baseProductInfo.productName
  ) {
    return {
      manufacturer: entity.manufacturerA,
      productName: entity.productNameA,
    };
  } else {
    return baseProductInfo;
  }
}

function removeDuplicatedReplacementsFilter(
  productInfo: ReplacementProductInfo,
  index: number,
  array: ReplacementProductInfo[],
): boolean {
  const isDuplicate = (array: ReplacementProductInfo[], productInfo: ReplacementProductInfo) => {
    return array.some(
      (item) => item.productName === productInfo.productName && productInfo.productName === productInfo.productName,
    );
  };
  return !isDuplicate(array.slice(0, index), productInfo);
}
