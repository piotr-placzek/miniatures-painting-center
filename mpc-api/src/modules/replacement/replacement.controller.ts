import { FastifyReply, FastifyRequest } from 'fastify';
import { GetReplacementsQuery } from './replacement.types';
import * as replacements from './replacement.service';

export async function searchReplacements(req: FastifyRequest, rep: FastifyReply) {
  const { productName, manufacturer, depth } = req.query as GetReplacementsQuery;
  rep.send(await replacements.find(productName, manufacturer, depth));
}
