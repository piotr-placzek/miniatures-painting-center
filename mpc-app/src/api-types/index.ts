export type Product = {
  id: string;
  manufacturer: string;
  name: string;
  description: string;
  image: string;
};

export type ProductSearchParams = {
  search?: string;
};

export type ReplacementsSearchParams = {
  manufacturer: string;
  productName: string;
  depth?: number;
};
