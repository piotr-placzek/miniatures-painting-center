import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'mpc-app';

  constructor(private readonly router: Router) {}

  navigateHome() {
    this.router.navigate(['/']);
  }
}
