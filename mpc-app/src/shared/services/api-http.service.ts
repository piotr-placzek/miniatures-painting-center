import { HttpClient, HttpContext, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

interface RequestOptions {
  headers?:
    | HttpHeaders
    | {
        [header: string]: string | string[];
      };
  context?: HttpContext;
  params?:
    | HttpParams
    | {
        [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>;
      };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

@Injectable()
export class ApiHttpService {
  constructor(private readonly http: HttpClient) {}

  private proxyUrl(url: string) {
    if (!url.startsWith('/api/') || isDevMode()) return url;
    return environment.api + url;
  }

  get<T>(url: string, options: RequestOptions): Observable<T> {
    return this.http.get<T>(this.proxyUrl(url), options);
  }
}
