import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/api-types';

@Component({
  selector: 'mpc-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
})
export class ProductsListComponent implements OnChanges {
  @Input() hideSearchingForm: boolean = false;
  @Input() products: any;
  @Output() search = new EventEmitter<string>();
  @ViewChild('items') items!: ElementRef;

  public searchInput = '';

  constructor(private renderer: Renderer2, private readonly router: Router) {}

  ngOnChanges(): void {
    const element = this.items.nativeElement;
    if (element) {
      const windowHeight = window.innerHeight;
      const elementOffsetTop = element.offsetTop;
      const availableHeight = windowHeight - elementOffsetTop;
      const height = (availableHeight * 100) / windowHeight;
      this.renderer.setStyle(element, 'height', `${height}vh`);
    }
  }

  clearSearchInput() {
    this.searchInput = '';
    this.search.emit('');
  }

  itemClicked(product: Product) {
    this.router.navigate(['/products', product.manufacturer, product.id]);
  }
}
