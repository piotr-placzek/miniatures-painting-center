import { Component, Input } from '@angular/core';

@Component({
  selector: 'mpc-products-list-item',
  templateUrl: './products-list-item.component.html',
  styleUrls: ['./products-list-item.component.scss'],
})
export class ProductsListItemComponent {
  @Input() product: any;
}
