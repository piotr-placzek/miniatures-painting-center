import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ApiHttpService } from './services/api-http.service';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductsListItemComponent } from './components/products-list/products-list-item/products-list-item.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

const COMPONENTS = [ProductsListComponent, ProductsListItemComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, FormsModule, HttpClientModule],
  providers: [ApiHttpService],
  exports: [CommonModule, ...COMPONENTS]
})
export class SharedModule {}
