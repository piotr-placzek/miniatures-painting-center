import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  standalone: true,
  name: 'titlecase',
})
export class TitleCaseString implements PipeTransform {
  transform(value: string): string {
    return value.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.slice(1).toLowerCase();
    });
  }
}
