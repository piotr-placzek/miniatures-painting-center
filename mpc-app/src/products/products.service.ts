import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product, ProductSearchParams } from 'src/api-types';
import { ApiHttpService } from 'src/shared/services/api-http.service';

@Injectable()
export class ProductsService {
  constructor(private readonly http: ApiHttpService) {}

  getProduct(id: string, manufacturer: string): Observable<Product> {
    return this.http.get<Product>(`/api/products/details`, { params: { id, manufacturer } });
  }

  getProducts(search?: string): Observable<Product[]> {
    const params: ProductSearchParams = {};
    if (search) {
      params.search = search;
    }
    return this.http.get<Product[]>(`/api/products`, { params });
  }

  getReplacements(manufacturer: string, productName: string): Observable<Product[]> {
    return this.http.get<Product[]>(`/api/replacements`, { params: { manufacturer, productName } });
  }
}
