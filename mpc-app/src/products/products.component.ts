import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, lastValueFrom, tap } from 'rxjs';
import { ProductsService } from './products.service';
import { Product } from 'src/api-types';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  templateUrl: './products.component.html',
})
export class ProductsComponent implements OnInit, OnDestroy {
  public product?: Product;
  public products: Product[] = [];

  private id?: string;
  private manufacturer?: string;

  private subscription: Subscription = new Subscription();

  constructor(private readonly service: ProductsService, private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    this.subscription.add(
      this.route.params.subscribe((params) => {
        this.id = params['id'];
        this.manufacturer = params['manufacturer'];
        this.init();
      }),
    );

    this.init();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  searchProducts(value: string) {
    lastValueFrom(
      this.service.getProducts(value).pipe(
        tap((data) => {
          this.products = data as Product[];
        }),
      ),
    );
  }

  private init() {
    if (this.id && this.manufacturer) {
      this.getProduct(this.id, this.manufacturer).then(() => {
        if (this.product) this.getReplacements(this.product);
      });
    } else {
      this.searchProducts('');
    }
  }

  private getProduct(id: string, manufacturer: string) {
    return lastValueFrom(
      this.service.getProduct(id, manufacturer).pipe(
        tap((data) => {
          this.product = data;
        }),
      ),
    );
  }

  private getReplacements(product: Product) {
    return lastValueFrom(
      this.service.getReplacements(product.manufacturer, product.name).pipe(
        tap((data) => {
          this.products = data;
        }),
      ),
    );
  }

  get hideSearchingForm() {
    return Boolean(this.id && this.manufacturer);
  }
}
