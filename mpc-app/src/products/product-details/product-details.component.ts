import { Component, INJECTOR, Input } from "@angular/core";
import { Product } from "src/api-types";

@Component({
    selector: 'mpc-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent {
    @Input() product!: Product
    @Input() replacementsCount!: number;
}
