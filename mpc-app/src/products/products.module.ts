import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/shared/shared.module';
import { ProductsComponent } from './products.component';
import { ProductsService } from './products.service';
import { ProductDetailsComponent } from './product-details/product-details.component';

@NgModule({
  declarations: [ProductsComponent, ProductDetailsComponent],
  imports: [SharedModule],
  providers: [ProductsService],
})
export class ProductsModule {}
