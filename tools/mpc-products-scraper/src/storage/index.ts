import { RawProduct, RawReplacementPair } from '../scrapers/scraper-base';
// export * as db from './database/sqlite';
export * as db from './database/mysql';

export interface StorageBase {
  insertProducts: (products: RawProduct[]) => Promise<void>;
  insertReplacements: (replacements: Omit<RawReplacementPair, 'id'>[]) => Promise<void>;
}

export const stdout: StorageBase = {
  insertProducts: async (products: RawProduct[]): Promise<void> => {
    console.table(products);
  },
  insertReplacements: async (replacements: Omit<RawReplacementPair, 'id'>[]) => {
    console.log(replacements);
  },
};
