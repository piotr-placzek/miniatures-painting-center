export interface ProductEntity {
  id: string;
  manufacturer: string;
  name: string;
  description: string;
  image: string;
}
