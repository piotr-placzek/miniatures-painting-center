import * as mysql from 'mysql2';
import { RawProduct, RawReplacementPair } from '../../scrapers/scraper-base';

const db = mysql.createConnection({
  host: 'localhost',
  user: 'user',
  password: 'password',
  database: 'db',
  port: 3366,
});

export async function insertProducts(products: RawProduct[]): Promise<void> {
  const values = products.map(
    (product: RawProduct) =>
      `('${product.id}','${product.manufacturer}','${product.name.replaceAll(
        "'",
        '',
      )}','${product.description.replaceAll("'", '')}','${product.image}')`,
  );
  db.query(`
    INSERT IGNORE INTO products (id, manufacturer, name, description, image)
    VALUES ${values.join(',')};
  `);
}

export async function upsertProducts(products: RawProduct[]): Promise<void> {
  const values = products
    .map(
      (product: RawProduct) =>
        `('${product.id}','${product.manufacturer}','${product.name.replaceAll(
          "'",
          '',
        )}','${product.description.replaceAll("'", '')}','${product.image}')`,
    )
    .join(',');
  db.query(
    `INSERT INTO products (id, manufacturer, name, description, image) VALUES ${values} ON DUPLICATE KEY UPDATE name = VALUES(name), description = VALUES(description);`,
  );
}

export async function insertReplacements(replacements: Omit<RawReplacementPair, 'id'>[]): Promise<void> {
  const values = replacements
    .map(
      (pair: Omit<RawReplacementPair, 'id'>) =>
        `('${pair.manufacturerA}','${pair.productNameA.replaceAll("'", '')}','${
          pair.manufacturerB
        }','${pair.productNameB.replaceAll("'", '')}','${pair.author}')`,
    )
    .join(',');
  db.query(
    `INSERT INTO replacements (manufacturerA, productNameA, manufacturerB, productNameB, author) VALUES ${values}`,
  );
}

export async function init(): Promise<void> {
  db.query(`
    CREATE TABLE IF NOT EXISTS products (
      id varchar(255) NOT NULL,
      manufacturer varchar(255) NOT NULL,
      name varchar(255) NOT NULL,
      description varchar(255),
      image varchar(255),
      PRIMARY KEY (id, manufacturer)
    );
  `);
  db.query(`
    CREATE TABLE IF NOT EXISTS replacements (
      id int NOT NULL AUTO_INCREMENT,
      manufacturerA varchar(255) NOT NULL,
      productNameA varchar(255) NOT NULL,
      manufacturerB varchar(255) NOT NULL,
      productNameB varchar(255) NOT NULL,
      author varchar(255) NOT NULL,
      PRIMARY KEY (id)
    );
  `);
}
