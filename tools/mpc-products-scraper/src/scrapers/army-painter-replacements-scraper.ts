import { StorageBase, db } from '../storage';
import { ReplacementPair } from './scraper-base';

type AP_CONVERSION_CHART_ENTRY = {
  ap: string;
  gw: string | null;
  va: string | null;
};

const CHART: AP_CONVERSION_CHART_ENTRY[] = [
  {
    ap: 'wizards orb',
    gw: 'kabalite green',
    va: 'jade green',
  },
  {
    ap: 'elemental bolt',
    gw: 'sybarite green',
    va: 'foul green',
  },
  {
    ap: 'toxic mist',
    gw: 'templeguard blue',
    va: null,
  },
  {
    ap: 'royal cloak',
    gw: 'baharroth blue',
    va: null,
  },
  {
    ap: 'hydra turquoise',
    gw: 'thousand sons blue',
    va: 'turquoise',
  },
  {
    ap: 'deep blue',
    gw: 'kantor blue',
    va: 'imperial blue',
  },
  {
    ap: 'griffon blue',
    gw: 'macagge blue',
    va: null,
  },
  {
    ap: 'ultramarine blue',
    gw: 'altdorf guard blue',
    va: 'ultramarine',
  },
  {
    ap: 'viking blue',
    gw: 'caledor sky',
    va: 'magic blue',
  },
  {
    ap: 'electric blue',
    gw: 'calgar blue',
    va: null,
  },
  {
    ap: 'ice storm',
    gw: 'blue horror',
    va: 'glacier blue',
  },
  {
    ap: 'voidshield blue',
    gw: 'hoeth blue',
    va: 'electric blue',
  },
  {
    ap: 'troglodyte blue',
    gw: 'lothern blue',
    va: null,
  },
  {
    ap: 'crystal blue',
    gw: 'teclis blue',
    va: 'andrea blue',
  },
  {
    ap: 'dark sky',
    gw: 'kantor blue',
    va: 'imperial blue',
  },
  {
    ap: 'wolf grey',
    gw: 'russ grey',
    va: 'sombre grey',
  },
  {
    ap: 'fog grey',
    gw: 'fenrisian grey',
    va: 'grey blue',
  },
  {
    ap: 'gorgon hide',
    gw: 'ulthuan grey',
    va: 'ghost grey',
  },
  {
    ap: 'matt black',
    gw: 'abaddon black',
    va: 'black',
  },
  {
    ap: 'necromancer cloak',
    gw: 'eshin grey',
    va: null,
  },
  {
    ap: 'dungeon grey',
    gw: 'dawnstone',
    va: 'cold grey',
  },
  {
    ap: 'field grey',
    gw: 'warpfield grey',
    va: null,
  },
  {
    ap: 'uniform grey',
    gw: 'dawnstone',
    va: 'cold grey',
  },
  {
    ap: 'castle grey',
    gw: 'stormvermin fur',
    va: 'heacy charcoal',
  },
  {
    ap: 'hardended carpace',
    gw: 'skavenblight dinge',
    va: null,
  },
  {
    ap: 'hemp rope',
    gw: 'balor brown',
    va: 'leather brown',
  },
  {
    ap: 'monster brown',
    gw: 'baneblade brown',
    va: 'heavy brown',
  },
  {
    ap: 'leather brown',
    gw: 'steel legion brown',
    va: 'earth',
  },
  {
    ap: 'dirt spatter',
    gw: 'mourfang brown',
    va: 'beasty brown',
  },
  {
    ap: 'oak brown',
    gw: 'dryard bark',
    va: 'charred brown',
  },
  {
    ap: 'basilisk brown',
    gw: 'zamesi desert',
    va: 'plague brown',
  },
  {
    ap: 'sulfide ochre',
    gw: 'zamesi desert',
    va: 'plague brown',
  },
  {
    ap: 'desert yellow',
    gw: 'zandri dust',
    va: 'khaki',
  },
  {
    ap: 'moon dust',
    gw: 'hexos palesun',
    va: 'pale yellow',
  },
  {
    ap: 'moon dust',
    gw: 'dorn yellow',
    va: 'pale yellow',
  },
  {
    ap: 'arid earth',
    gw: 'dorn yellow',
    va: 'pale yellow',
  },
  {
    ap: 'babe blonde',
    gw: null,
    va: 'moon yellow',
  },
  {
    ap: 'daemonic yellow',
    gw: 'flash gitz yellow',
    va: 'sun yellow',
  },
  {
    ap: 'phoenix flames',
    gw: 'yriel yellow',
    va: 'gold yellow',
  },
  {
    ap: 'fire lizard',
    gw: 'jokaero orange',
    va: 'scrofulous brown',
  },
  {
    ap: 'lava orange',
    gw: 'trollslayer orange',
    va: 'hot orange',
  },
  {
    ap: 'mythical orange',
    gw: 'fire dragon bright',
    va: 'orange fire',
  },
  {
    ap: 'mars red',
    gw: 'wildrider red',
    va: null,
  },
  {
    ap: 'pure red',
    gw: 'evil sunz scarlet',
    va: 'bloody red',
  },
  {
    ap: 'dragon red',
    gw: 'wazdakka red',
    va: 'gory red',
  },
  {
    ap: 'abomination gore',
    gw: 'khone red',
    va: 'scarlet red',
  },
  {
    ap: 'vampire red',
    gw: 'mephistion red',
    va: 'heavy red',
  },
  {
    ap: 'crusted sore',
    gw: 'khorne red',
    va: null,
  },
  {
    ap: 'wasteland soil',
    gw: 'screamer pink',
    va: 'warlord purple',
  },
  {
    ap: 'grimoire purple',
    gw: 'warpfiend grey',
    va: 'hexed lichen',
  },
  {
    ap: 'grimoire purple',
    gw: 'daemonette hide',
    va: 'hexed lichen',
  },
  {
    ap: 'dark stone',
    gw: 'mechanicus standard grey',
    va: 'heavy grey',
  },
  {
    ap: 'cultist robe',
    gw: 'baneblade brown',
    va: 'heavy brown',
  },
  {
    ap: 'werewolf fur',
    gw: 'gothor brown',
    va: null,
  },
  {
    ap: 'banshee brown',
    gw: 'rakarth flesh',
    va: 'heavy warm grey',
  },
  {
    ap: 'brainmatter beige',
    gw: 'screaming skull',
    va: 'offwhite',
  },
  {
    ap: 'spaceship exterior',
    gw: 'ulthuan grey',
    va: 'cold grey',
  },
  {
    ap: 'stone golem',
    gw: 'pallid wych flesh',
    va: null,
  },
  {
    ap: 'matt white',
    gw: 'white scar',
    va: 'dead white',
  },
  {
    ap: 'matt white',
    gw: 'ceramite',
    va: 'dead white',
  },
  {
    ap: 'ash grey',
    gw: 'administorum grey',
    va: 'stonewall grey',
  },
  {
    ap: 'oozing purple',
    gw: 'genestealer purple',
    va: null,
  },
  {
    ap: 'alien purple',
    gw: 'xereus purple',
    va: 'royal purple',
  },
  {
    ap: 'warlock purple',
    gw: 'pink horror',
    va: null,
  },
  {
    ap: 'pixie pink',
    gw: 'emperors children',
    va: 'squid pink',
  },
  {
    ap: 'centaur skin',
    gw: 'fulgrim pink',
    va: null,
  },
  {
    ap: 'scar tissue',
    gw: 'bugmans glow',
    va: 'tan',
  },
  {
    ap: 'tanned flesh',
    gw: 'ratskin flesh',
    va: 'dwarf skin',
  },
  {
    ap: 'fur brown',
    gw: 'skrag brown',
    va: 'parasite brown',
  },
  {
    ap: 'troll claws',
    gw: 'tau light ochre',
    va: 'filthy brown',
  },
  {
    ap: 'barbarian flesh',
    gw: 'bestigor flesh',
    va: 'scrofulous brown',
  },
  {
    ap: 'kobold skin',
    gw: 'cadian fleshstone',
    va: 'heavy skintone',
  },
  {
    ap: 'elven flesh',
    gw: 'kislev',
    va: 'pale flesh',
  },
  {
    ap: 'elven flesh',
    gw: 'ungor flesh',
    va: 'pale flesh',
  },
  {
    ap: 'corpse pale',
    gw: 'flayed one flesh',
    va: 'elfic flesh',
  },
  {
    ap: 'toxic boils',
    gw: 'slaanesh grey',
    va: null,
  },
  {
    ap: 'skeleton bone',
    gw: 'ushtabi bone',
    va: 'bone white',
  },
  {
    ap: 'mummy robes',
    gw: 'pallid wych flesh',
    va: null,
  },
  {
    ap: 'drake tooth',
    gw: 'screaming skull',
    va: 'bone white',
  },
  {
    ap: 'necrotic flesh',
    gw: 'kreig khaki',
    va: 'dead flesh',
  },
  {
    ap: 'combat fatigues',
    gw: 'nurgling green',
    va: 'dead fleash',
  },
  {
    ap: 'venom wyrm',
    gw: 'death guard green',
    va: 'yellow olive',
  },
  {
    ap: 'filthy cape',
    gw: 'stormvermin fur',
    va: null,
  },
  {
    ap: 'crypt wraith',
    gw: 'castellan green',
    va: 'cayman green',
  },
  {
    ap: 'elf green',
    gw: 'deathworld forest',
    va: 'camo green',
  },
  {
    ap: 'commando green',
    gw: 'elysian green',
    va: 'camo green',
  },
  {
    ap: 'witch brew',
    gw: 'ogryn camo',
    va: null,
  },
  {
    ap: 'scaly hide',
    gw: 'kreig khaki',
    va: null,
  },
  {
    ap: 'goblin green',
    gw: 'warboss green',
    va: 'goblin green',
  },
  {
    ap: 'kraken skin',
    gw: 'gauss blaster green',
    va: null,
  },
  {
    ap: 'greenskin',
    gw: 'warpstone glow',
    va: 'sick green',
  },
  {
    ap: 'angel green',
    gw: 'caliban green',
    va: 'sick green',
  },
  {
    ap: 'mouldy clothes',
    gw: 'skarsnik green',
    va: 'scorpy green',
  },
  {
    ap: 'army green',
    gw: 'straken green',
    va: 'yellow olive',
  },
  {
    ap: 'snake green',
    gw: 'moot green',
    va: 'scorpy green',
  },
  {
    ap: 'jungle green',
    gw: 'niblet green',
    va: 'livery green',
  },
  {
    ap: 'poisonous cloud',
    gw: null,
    va: 'yellow fluorescent',
  },
  {
    ap: 'poisonous cloud',
    gw: '',
    va: 'green fluorescent',
  },
  {
    ap: 'dry rust',
    gw: 'ryza rust',
    va: 'rust',
  },
  {
    ap: 'dry rust',
    gw: 'ryza rust',
    va: 'dry rust',
  },
  {
    ap: 'glistening blood',
    gw: 'blood for the blood god',
    va: 'fresh blood',
  },
  {
    ap: 'wet mud',
    gw: 'typhus corrosion',
    va: null,
  },
  {
    ap: 'disgusting slime',
    gw: 'nurgles rot',
    va: 'vomit',
  },
  {
    ap: 'rough iron',
    gw: 'warlock bronze',
    va: 'tinny tin',
  },
  {
    ap: 'gun metal',
    gw: 'leadbelcher',
    va: 'gunmetal',
  },
  {
    ap: 'plate mail metal',
    gw: 'ironbreaker',
    va: 'chainmail silver',
  },
  {
    ap: 'shining silver',
    gw: 'runefang',
    va: 'silver',
  },
  {
    ap: 'shining silver',
    gw: 'stormhost silver',
    va: 'silver',
  },
  {
    ap: 'weapon bronze',
    gw: 'brass scorpion',
    va: 'bright brass',
  },
  {
    ap: 'true copper',
    gw: 'balthazar gold',
    va: 'hammered copper',
  },
  {
    ap: 'greedy gold',
    gw: 'gehenna gold',
    va: 'glorious gold',
  },
  {
    ap: 'bright gold',
    gw: 'auric armour gold',
    va: 'polished gold',
  },
  {
    ap: 'red tone',
    gw: 'carroburg crimson',
    va: 'red wash',
  },
  {
    ap: 'flesh wash',
    gw: 'reikland fleshshade',
    va: 'flesh wash',
  },
  {
    ap: 'ligh tone',
    gw: 'seraphim sepia',
    va: 'sepia wash',
  },
  {
    ap: 'dark tone',
    gw: 'nuln oil',
    va: 'black wash',
  },
  {
    ap: 'mid brown',
    gw: 'agrax earthshade',
    va: 'umber wash',
  },
  {
    ap: 'mid brown',
    gw: 'reikland fleshshade',
    va: 'umber wash',
  },
  {
    ap: 'strong tone',
    gw: 'agrax earthshade',
    va: 'umber wash',
  },
  {
    ap: 'purple tone',
    gw: 'druchii violet',
    va: null,
  },
  {
    ap: 'green tone',
    gw: 'biel-tan green',
    va: 'green wash',
  },
  {
    ap: 'military shader',
    gw: 'athonian camoshade',
    va: null,
  },
  {
    ap: 'blue tone',
    gw: 'drakenhof nightshade',
    va: 'blue wash',
  },
  {
    ap: 'soft tone',
    gw: 'seraphim sepia',
    va: 'sepia',
  },
  {
    ap: 'soft tone',
    gw: 'seraphim sepia',
    va: 'umber wash',
  },
  {
    ap: 'soft tone',
    gw: 'agrax earthshade',
    va: 'sepia',
  },
  {
    ap: 'soft tone',
    gw: 'agrax earthshade',
    va: 'umber wash',
  },
];

function buildProductsPairs(x: AP_CONVERSION_CHART_ENTRY[]): ReplacementPair[] {
  return x.reduce((acc: ReplacementPair[], curr: AP_CONVERSION_CHART_ENTRY): ReplacementPair[] => {
    if (curr.gw) {
      acc.push({
        manufacturerA: 'army-painter',
        productNameA: curr.ap,
        manufacturerB: 'games-workshop',
        productNameB: curr.gw,
        author: 'army-painter',
      });
    }

    if (curr.va) {
      acc.push({
        manufacturerA: 'army-painter',
        productNameA: curr.ap,
        manufacturerB: 'vallejo-acrylic',
        productNameB: curr.va,
        author: 'army-painter',
      });
    }

    if (curr.gw && curr.va) {
      acc.push({
        manufacturerA: 'games-workshop',
        productNameA: curr.gw,
        manufacturerB: 'vallejo-acrylic',
        productNameB: curr.va,
        author: 'army-painter',
      });
    }

    return acc;
  }, []);
}

function removeDuplicates(productsPairs: ReplacementPair[]): ReplacementPair[] {
    const isDuplicate = (arr: ReplacementPair[], obj: ReplacementPair) => {
      return arr.some(
        (item: ReplacementPair) =>
          (item.productNameA === obj.productNameA && item.productNameB === obj.productNameB) ||
          (item.productNameA === obj.productNameB && item.productNameB === obj.productNameA),
      );
    };
    return productsPairs.filter((item, index, self) => !isDuplicate(self.slice(0, index), item));
}

function seed(db: StorageBase, chartData: AP_CONVERSION_CHART_ENTRY[]) {
  db.insertReplacements(removeDuplicates(buildProductsPairs(chartData)));
}

seed(db, CHART);
