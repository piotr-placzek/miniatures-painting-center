import * as puppeteer from 'puppeteer';
import { db } from '../storage';
import { RawProduct, scrape } from './scraper-base';

interface VallejoAcrylicProduct extends RawProduct {
  manufacturer: 'vallejo-acrylic';
}

const ITEM_SELECTOR = 'span.image-placeholder > img.attachment-woocommerce_thumbnail.size-woocommerce_thumbnail';

const URL = {
  MODEL_COLOR: 'https://acrylicosvallejo.com/en/category/hobby/model-color-en/',
  // GAME_COLOR: 'https://acrylicosvallejo.com/en/category/hobby/game-color-en/',
  // AIR_COLOR: 'https://acrylicosvallejo.com/en/category/model-air-en/',
};

const PAGE_CNT: { [key: string]: number } = {
  MODEL_COLOR: 11,
  // GAME_COLOR: 8,
  // AIR_COLOR: 14,
};

interface ListElement extends Element {}

function sourceUrlsFactory(): string[] {
  const sourceUrls: string[] = [];
  for (const [key, value] of Object.entries(URL)) {
    if (!Object.keys(PAGE_CNT).includes(key)) {
      sourceUrls.push(value);
    } else {
      sourceUrls.push(value);
      for (let i = 1, j = 2; i < PAGE_CNT[key]; i++, j++) {
        sourceUrls.push(value + 'page/' + j + '/');
      }
    }
  }
  return sourceUrls;
}

async function scrapingStrategy(page: puppeteer.Page): Promise<any[]> {
  return page.evaluate((ITEM_SELECTOR: string): any[] => {
    const products: VallejoAcrylicProduct[] = [];
    const extractDataSrcBase = (dataSrc: string): string => {
      return dataSrc
        .split('/')
        .slice(-1)[0]
        .replace(/-Rev\d+/, '')
        .replace(/-\d+x\d+/, '')
        .replace(/\..+/, '');
    };
    const extractId = (dataSrc: string): string => {
      return extractDataSrcBase(dataSrc).split('-').slice(-1)[0];
    };
    const extractName = (dataSrc: string): string => {
      return extractDataSrcBase(dataSrc).split('-vallejo-')[1].split('-').slice(0, -1).join(' ');
    };
    const extractDescription = (dataSrc: string): string => {
      return extractDataSrcBase(dataSrc).split('-vallejo-')[0].split('-').join(' ');
    };

    const toTitleCase = (str: string) =>
      str.replace(/(^\w|\s\w)(\S*)/g, (_, m1, m2) => m1.toUpperCase() + m2.toLowerCase());

    const nodeListOfElements: NodeListOf<ListElement> = document.querySelectorAll(ITEM_SELECTOR);

    for (let element of Array.from(nodeListOfElements)) {
      const dataSrc: string = element.getAttribute('data-src')!;
      products.push({
        id: toTitleCase(extractId(dataSrc)),
        name: toTitleCase(extractName(dataSrc)),
        image: dataSrc,
        description: toTitleCase(extractDescription(dataSrc)),
        manufacturer: 'vallejo-acrylic',
      });
    }

    return products;
  }, ITEM_SELECTOR);
}

scrape(sourceUrlsFactory(), scrapingStrategy, ITEM_SELECTOR, db);
