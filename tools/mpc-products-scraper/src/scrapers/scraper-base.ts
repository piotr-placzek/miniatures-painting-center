import * as puppeteer from 'puppeteer';

type Manufacturer = 'games-workshop' | 'army-painter' | 'vallejo-acrylic';

export type RawProduct = {
  id: string;
  name: string;
  description: string;
  image: string;
  manufacturer: Manufacturer;
}

export type RawReplacementPair = {
  id: number;
  manufacturerA: Manufacturer;
  manufacturerB: Manufacturer;
  productNameA: string;
  productNameB: string;
  author: string;
}

export type ReplacementPair = Omit<RawReplacementPair, 'id'>;

export type ScrapingStrategy = (page: puppeteer.Page) => Promise<RawProduct[]>;

interface StorageBase {
  insertProducts: (products: RawProduct[]) => Promise<void>;
}

export async function scrape(
  sourceUrls: string[],
  strategy: ScrapingStrategy,
  itemSelector: string,
  storage: StorageBase,
): Promise<void> {
  try {
    const products = await getAllProducts(sourceUrls, strategy, itemSelector);
    if (!products.length) return;
    storage.insertProducts(
      products.map((v: RawProduct) => ({
        ...v,
        name: v.name.toLowerCase(),
        description: v.description.toLowerCase(),
      })),
    );
  } catch (error) {
    console.log(error);
  }
}

async function getAllProducts(
  sourceUrls: string[],
  strategy: ScrapingStrategy,
  itemSelector: string,
): Promise<RawProduct[]> {
  const allProducts: RawProduct[] = [];

  for (let sourceUrl of sourceUrls) {
    allProducts.push(...(await getProducts(sourceUrl, strategy, itemSelector)));
  }

  console.log('Products found:', allProducts.length);
  return allProducts;
}

async function getProducts(sourceUrl: string, strategy: ScrapingStrategy, itemSelector: string): Promise<RawProduct[]> {
  const browser: puppeteer.Browser = await puppeteer.launch({ headless: false });
  const page: puppeteer.Page = await browser.newPage();
  await page.goto(sourceUrl);
  await page.waitForSelector(itemSelector, { visible: true });
  const products: RawProduct[] = await strategy(page);
  await browser.close();
  return products.filter((product: RawProduct) => product.id);
}
